﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Timers;
using CharmsBarToggle.Charms;
using System.IO;
using System.Reflection;

namespace CharmsBarToggle
{
    public partial class ToggleForm : Form
    {
        /// <summary>
        /// Class that defines whether the bar and clock are visible or not.
        /// </summary>
        private class CharmsStatus
        {
            public Boolean BarVisible;
            public Boolean ClockVisible;
        }

        /// <summary>
        /// Name of they key for the toggle
        /// </summary>
        private const String STARTUP_KEY_NAME = "CharmsBarToggle";

        /// <summary>
        /// Identifer for the hide bar on startup flag
        /// </summary>
        public const String HIDE_BAR_ON_STARTUP_FLAG = "--hide-charms-bar";

        /// <summary>
        /// Identifier for the hide clock flag
        /// </summary>
        public const String HIDE_CLOCK_ON_STARTUP_FLAG = "--hide-charms-clock";

        /// <summary>
        /// Identifier to hide the menu after starting
        /// </summary>
        public const String AUTO_MINIMIZE_AFTER_STARTUP_FLAG = "--auto-minimize-after-startup";

        /// <summary>
        /// Filename of the log.
        /// </summary>
        public const String CHARMS_LOG_FILENAME = "CharmsBarToggle.log";

        /// <summary>
        /// Expected status of the bar and clock
        /// </summary>
        private CharmsStatus _ExpectedStatus;

        /// <summary>
        /// Key used to store the startup options for the toggle
        /// </summary>
        private RegistryKey _StartupKey;

        /// <summary>
        /// Whether to minimize after starting up
        /// </summary>
        private Boolean _MinimizeAfterStartup;

        /// <summary>
        /// Menu used in the tray as quick access to visibility settings.
        /// </summary>
        private ContextMenu _NotificationMenu;

        /// <summary>
        /// Menu item to restore the window.
        /// </summary>
        private MenuItem _RestoreNotificationItem;

        /// <summary>
        /// Menu item used to show both the clock and bar
        /// </summary>
        private MenuItem _ShowAllNotificationItem;

        /// <summary>
        /// Menu item used to hide both the clock and bar
        /// </summary>
        private MenuItem _HideAllNotificationItem;

        /// <summary>
        /// Menu item used to show the bar
        /// </summary>
        private MenuItem _ShowBarNotificationItem;

        /// <summary>
        /// Menu item used to hide the bar
        /// </summary>
        private MenuItem _HideBarNotificationItem;

        /// <summary>
        /// Menu item used to show the clock
        /// </summary>
        private MenuItem _ShowClockNotificationItem;

        /// <summary>
        /// Menu item used to hide the clock
        /// </summary>
        private MenuItem _HideClockNotificationItem;

        /// <summary>
        /// Menu item used to exit the application
        /// </summary>
        private MenuItem _ExitNotificationItem;

        /// <summary>
        /// List containing the individual log messages
        /// </summary>
        private List<String> _Logs;

        public ToggleForm(bool hideBarAfterStartup, bool hideClockAfterStartup, bool minimizeAfterStartup)
        {
            InitializeComponent();

            if (hideBarAfterStartup)
            {
                this.ExpectedStatus.BarVisible = false;
            }
            else
            {
                this.ExpectedStatus.BarVisible = Charms.Charms.Bar.Visible;
            }

            if (hideClockAfterStartup)
            {
                this.ExpectedStatus.ClockVisible = false;
            }
            else
            {
                this.ExpectedStatus.ClockVisible = Charms.Charms.Clock.Visible;
            }

            this.MinimizeAfterStartup = minimizeAfterStartup;

            Charms.Charms.Bar.PropertyChanged += this.HandleBarPropertyChange;
            Charms.Charms.Clock.PropertyChanged += this.HandleClockPropertyChange;
        }

        /// <summary>
        /// Writes a message to the log textbox
        /// </summary>
        /// <param name="value"></param>
        protected void Log(String value)
        {
            String message = String.Format("[{0:MM/dd/yy H:mm:ss}]{1}", DateTime.Now, value);
            this.Logs.Add(message);

            this.LogTextBox.AppendText(message + "\n");

            try
            {
                String logPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "CharmsBarToggle.log");

                if (File.Exists(logPath))
                {
                    FileInfo logInfo = new FileInfo(logPath);

                    if (logInfo.Length > (10 * 1024 * 1024))
                    {
                        String newPath = String.Format("{0}.{1:MM-dd-yy H_mm_ss}", logPath, DateTime.Now);
                        File.Move(logPath, newPath);
                    }
                }

                StreamWriter logFile = new StreamWriter(File.Open(logPath, FileMode.Append));
                logFile.WriteLine(message);
                logFile.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// Handles when the bar has a property change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="name"></param>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void HandleBarPropertyChange(object sender, String name, Object oldValue, Object newValue)
        {
            if (this.InvokeRequired)
            {
                Invoke((MethodInvoker)delegate { this.HandleBarPropertyChange(sender, name, oldValue, newValue); });
                return;
            }

            this.Log(String.Format("Bar Property Changed[{0}]:{1}", name, newValue));

            if (name.Equals("Visible"))
            {
                Boolean visible = (Boolean)newValue;
                if (visible)
                {
                    this.ShowBarRadio.Checked = true;
                    this.ShowBarRadio.Enabled = false;

                    this.HideBarRadio.Checked = false;
                    this.HideBarRadio.Enabled = true;

                    this.ShowBarNotificationItem.Visible = false;
                    this.HideBarNotificationItem.Visible = true;
                }
                else
                {
                    this.ShowBarRadio.Checked = false;
                    this.ShowBarRadio.Enabled = true;

                    this.HideBarRadio.Checked = true;
                    this.HideBarRadio.Enabled = false;

                    this.ShowBarNotificationItem.Visible = true;
                    this.HideBarNotificationItem.Visible = false;
                }

                this.UpdateAllShowHideControls();

                if (!visible == this.ExpectedStatus.BarVisible)
                {
                    this.Log(String.Format("Bar Visibility not expected:{0}=={1}", newValue, this.ExpectedStatus.BarVisible));
                    Charms.Charms.Bar.Visible = this.ExpectedStatus.BarVisible;
                }
            }
        }

        /// <summary>
        /// Handles when the bar has a property change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="name"></param>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void HandleClockPropertyChange(object sender, String name, Object oldValue, Object newValue)
        {
            if (this.InvokeRequired)
            {
                Invoke((MethodInvoker)delegate { this.HandleClockPropertyChange(sender, name, oldValue, newValue); });
                return;
            }

            this.Log(String.Format("Clock Property Changed[{0}]:{1}", name, newValue));

            if (name.Equals("Visible"))
            {
                Boolean visible = (Boolean)newValue;
                if (visible)
                {
                    this.ShowClockRadio.Checked = true;
                    this.ShowClockRadio.Enabled = false;

                    this.HideClockRadio.Checked = false;
                    this.HideClockRadio.Enabled = true;

                    this.ShowClockNotificationItem.Visible = false;
                    this.HideClockNotificationItem.Visible = true;
                }
                else
                {
                    this.ShowClockRadio.Checked = false;
                    this.ShowClockRadio.Enabled = true;

                    this.HideClockRadio.Checked = true;
                    this.HideClockRadio.Enabled = false;

                    this.ShowClockNotificationItem.Visible = true;
                    this.HideClockNotificationItem.Visible = false;
                }
                
                this.UpdateAllShowHideControls();

                if (!visible == this.ExpectedStatus.ClockVisible)
                {
                    this.Log(String.Format("Clock Visibility not expected:{0}=={1}", newValue, this.ExpectedStatus.ClockVisible));
                    Charms.Charms.Clock.Visible = this.ExpectedStatus.ClockVisible;
                }
            }
        }

        /// <summary>
        /// Updates the controls based on the expected status
        /// </summary>
        private void UpdateAllShowHideControls()
        {
            if (Charms.Charms.Bar.Visible && Charms.Charms.Clock.Visible)
            {
                this.ShowAllRadio.Enabled = false;
                this.ShowAllRadio.Checked = true;

                this.HideAllRadio.Enabled = true;
                this.HideAllRadio.Checked = false;

                this.ShowAllNotificationItem.Visible = false;
                this.HideAllNotificationItem.Visible = true;
            }
            else if (!Charms.Charms.Bar.Visible && !Charms.Charms.Clock.Visible)
            {
                this.ShowAllRadio.Enabled = true;
                this.ShowAllRadio.Checked = false;

                this.HideAllRadio.Enabled = false;
                this.HideAllRadio.Checked = true;

                this.ShowAllNotificationItem.Visible = true;
                this.HideAllNotificationItem.Visible = false;
            }
            else
            {
                this.ShowAllRadio.Enabled = true;
                this.ShowAllRadio.Checked = false;

                this.HideAllRadio.Enabled = true;
                this.HideAllRadio.Checked = false;

                this.ShowAllNotificationItem.Visible = true;
                this.HideAllNotificationItem.Visible = true;
            }
        }

        /// <summary>
        /// Handles showing the bar and clock and updating the expected status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleShowAll(object sender, EventArgs e)
        {
            this.Log("Showing All");
            this.ExpectedStatus.BarVisible = true;
            this.ExpectedStatus.ClockVisible = true;

            Charms.Charms.Bar.Visible = true;
            Charms.Charms.Clock.Visible = true;
        }

        /// <summary>
        /// Handles hiding the bar and clock and updating the expected status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleHideAll(object sender, EventArgs e)
        {
            this.Log("Hiding All");
            this.ExpectedStatus.BarVisible = false;
            this.ExpectedStatus.ClockVisible = false;

            Charms.Charms.Bar.Visible = false;
            Charms.Charms.Clock.Visible = false;
        }

        /// <summary>
        /// Handles showing the bar and updating the expected status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleShowBar(object sender, EventArgs e)
        {
            this.Log("Showing Bar");
            this.ExpectedStatus.BarVisible = true;
            Charms.Charms.Bar.Visible = true;
        }

        /// <summary>
        /// Handles hiding the bar and updating the expected status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleHideBar(object sender, EventArgs e)
        {
            this.Log("Hiding Bar");
            this.ExpectedStatus.BarVisible = false;
            Charms.Charms.Bar.Visible = false;
        }

        /// <summary>
        /// Handles showing the clock and updating the expected status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleShowClock(object sender, EventArgs e)
        {
            this.Log("Showing Clock");
            this.ExpectedStatus.ClockVisible = true;
            Charms.Charms.Clock.Visible = true;
        }

        /// <summary>
        /// Handles hiding the clock and updating the expected status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleHideClock(object sender, EventArgs e)
        {
            this.Log("Hiding Clock");
            this.ExpectedStatus.ClockVisible = false;
            Charms.Charms.Clock.Visible = false;
        }

        private void ToggleForm_Load(object sender, EventArgs e)
        {
            //initializing the notification menu
            if (this.NotificationMenu != null) { };

            Charms.Charms.Bar.Visible = this.ExpectedStatus.BarVisible;
            Charms.Charms.Clock.Visible = this.ExpectedStatus.ClockVisible;

            this.HandleBarPropertyChange(Charms.Charms.Bar, "Visible", !this.ExpectedStatus.BarVisible, this.ExpectedStatus.BarVisible);
            this.HandleClockPropertyChange(Charms.Charms.Clock, "Visible", !this.ExpectedStatus.ClockVisible, this.ExpectedStatus.ClockVisible);
            this.UpdateAllShowHideControls();

            //rebuilding the startup options
            String startupValue = (String)this.StartupKey.GetValue(STARTUP_KEY_NAME, null);
            if (startupValue != null)
            {
                this.RunAtStartupCheckbox.Checked = true;

                if (startupValue.IndexOf(HIDE_BAR_ON_STARTUP_FLAG) != -1)
                {
                    this.HideBarAfterStartupCheckbox.Checked = true;
                }

                if (startupValue.IndexOf(HIDE_CLOCK_ON_STARTUP_FLAG) != -1)
                {
                    this.HideClockAfterStartupCheckbox.Checked = true;
                }

                if (startupValue.IndexOf(AUTO_MINIMIZE_AFTER_STARTUP_FLAG) != -1)
                {
                    this.MinimizeAfterStartupCheckbox.Checked = true;
                }
            }
            else
            {
                this.RunAtStartupCheckbox.Checked = false;
            }

            if (this.MinimizeAfterStartupCheckbox.Checked)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void ToggleForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.notifyIcon.BalloonTipTitle = "Charms Bar Toggle";
                this.notifyIcon.BalloonTipText = "Charms Bar Toggle is still running.";
                this.notifyIcon.Visible = true;
                this.notifyIcon.ShowBalloonTip(5000);
                this.ShowInTaskbar = false;
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.notifyIcon.Visible = false;
                this.ShowInTaskbar = true;
                this.LogTextBox.Text = "";

                foreach (String message in this.Logs)
                {
                    this.LogTextBox.AppendText(message + "\n");
                }
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void ExitNotificationItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void RestoreNotificationItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// Handles rebuilding the startup entry depending on what checkboxes are checked.
        /// </summary>
        private void RebuildStartupValue()
        {
            if (this.RunAtStartupCheckbox.Checked)
            {
                String value = Assembly.GetEntryAssembly().Location;

                if (this.HideBarAfterStartupCheckbox.Checked)
                {
                    value += " " + HIDE_BAR_ON_STARTUP_FLAG;
                }

                if (this.HideClockAfterStartupCheckbox.Checked)
                {
                    value += " " + HIDE_CLOCK_ON_STARTUP_FLAG;
                }

                if (this.MinimizeAfterStartupCheckbox.Checked)
                {
                    value += " " + AUTO_MINIMIZE_AFTER_STARTUP_FLAG;
                }

                this.Log("Setting Startup Entry:" + value);
                this.StartupKey.SetValue(STARTUP_KEY_NAME, value);
            }
            else
            {
                if (this.StartupKey.GetValue(STARTUP_KEY_NAME, null) != null)
                {
                    this.Log("Deleting Startup Entry:" + this.StartupKey.GetValue(STARTUP_KEY_NAME));
                    this.StartupKey.DeleteValue(STARTUP_KEY_NAME);
                }
            }
        }

        /// <summary>
        /// Handles updating the startup entry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunAtStartupCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.RunAtStartupCheckbox.Checked)
            {
                this.MinimizeAfterStartupCheckbox.Enabled = true;
                this.HideBarAfterStartupCheckbox.Enabled = true;
                this.HideClockAfterStartupCheckbox.Enabled = true;
            }
            else
            {
                this.MinimizeAfterStartupCheckbox.Enabled = false;
                this.MinimizeAfterStartupCheckbox.Checked = false;
                this.HideBarAfterStartupCheckbox.Enabled = false;
                this.HideBarAfterStartupCheckbox.Checked = false;
                this.HideClockAfterStartupCheckbox.Enabled = false;
                this.HideClockAfterStartupCheckbox.Checked = false;
            }

            this.RebuildStartupValue();
        }

        /// <summary>
        /// Common callback for all of the startup entry options to change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleRebuildStartupValue(object sender, EventArgs e)
        {
            this.RebuildStartupValue();
        }

        /*********************************************** Getters and Setters ******************************************************/

        /// <summary>
        /// Expected status of the bar and clock
        /// </summary>
        private CharmsStatus ExpectedStatus
        {
            get
            {
                if (this._ExpectedStatus == null)
                {
                    this._ExpectedStatus = new CharmsStatus();
                    this.ExpectedStatus.BarVisible = true;
                    this.ExpectedStatus.ClockVisible = true;
                }

                return this._ExpectedStatus;
            }
        }

        /// <summary>
        /// Key used to store the startup options for the toggle
        /// </summary>
        private RegistryKey StartupKey
        {
            get
            {
                if (this._StartupKey == null)
                {
                    this._StartupKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                }

                return this._StartupKey;
            }
        }

        /// <summary>
        /// Whether to minimize after starting up
        /// </summary>
        private Boolean MinimizeAfterStartup
        {
            get
            {
                return this._MinimizeAfterStartup;
            }
            set
            {
                this._MinimizeAfterStartup = value;
            }
        }

        /// <summary>
        /// Menu used in the tray as quick access to visibility settings.
        /// </summary>
        private ContextMenu NotificationMenu
        {
            get
            {
                if (this._NotificationMenu == null)
                {
                    this._NotificationMenu = new ContextMenu();
                    this.NotificationMenu.MenuItems.Add(this.RestoreNotificationItem);
                    this.NotificationMenu.MenuItems.Add("-");
                    this.NotificationMenu.MenuItems.Add(this.ShowAllNotificationItem);
                    this.NotificationMenu.MenuItems.Add(this.HideAllNotificationItem);
                    this.NotificationMenu.MenuItems.Add("-");
                    this.NotificationMenu.MenuItems.Add(this.ShowBarNotificationItem);
                    this.NotificationMenu.MenuItems.Add(this.HideBarNotificationItem);
                    this.NotificationMenu.MenuItems.Add(this.ShowClockNotificationItem);
                    this.NotificationMenu.MenuItems.Add(this.HideClockNotificationItem);
                    this.NotificationMenu.MenuItems.Add("-");
                    this.NotificationMenu.MenuItems.Add(this.ExitNotificationItem);

                    this.notifyIcon.ContextMenu = this.NotificationMenu;
                }

                return this._NotificationMenu;
            }
        }

        /// <summary>
        /// Menu item to restore the window.
        /// </summary>
        private MenuItem RestoreNotificationItem
        {
            get
            {
                if (this._RestoreNotificationItem == null)
                {
                    this._RestoreNotificationItem = new MenuItem();
                    this.RestoreNotificationItem.Text = "Restore Window";
                    this.RestoreNotificationItem.Click += this.RestoreNotificationItem_Click;
                }

                return this._RestoreNotificationItem;
            }
        }

        /// <summary>
        /// Menu item used to show both the clock and bar
        /// </summary>
        private MenuItem ShowAllNotificationItem
        {
            get
            {
                if (this._ShowAllNotificationItem == null)
                {
                    this._ShowAllNotificationItem = new MenuItem();
                    this.ShowAllNotificationItem.Text = "Show All";
                    this.ShowAllNotificationItem.Click += new EventHandler(this.HandleShowAll);
                }

                return this._ShowAllNotificationItem;
            }
        }

        /// <summary>
        /// Menu item used to show both the clock and bar
        /// </summary>
        private MenuItem HideAllNotificationItem
        {
            get
            {
                if (this._HideAllNotificationItem == null)
                {
                    this._HideAllNotificationItem = new MenuItem();
                    this.HideAllNotificationItem.Text = "Hide All";
                    this.HideAllNotificationItem.Click += new EventHandler(this.HandleHideAll);
                }

                return this._HideAllNotificationItem;
            }
        }

        /// <summary>
        /// Menu item used to show the bar
        /// </summary>
        private MenuItem ShowBarNotificationItem
        {
            get
            {
                if (this._ShowBarNotificationItem == null)
                {
                    this._ShowBarNotificationItem = new MenuItem();
                    this._ShowBarNotificationItem.Text = "Show Bar";
                    this.ShowBarNotificationItem.Click += new EventHandler(this.HandleShowBar);
                }

                return this._ShowBarNotificationItem;
            }
        }

        /// <summary>
        /// Menu item used to hide the bar
        /// </summary>
        private MenuItem HideBarNotificationItem
        {
            get
            {
                if (this._HideBarNotificationItem == null)
                {
                    this._HideBarNotificationItem = new MenuItem();
                    this._HideBarNotificationItem.Text = "Hide Bar";
                    this.HideBarNotificationItem.Click += new EventHandler(this.HandleHideBar);
                }

                return this._HideBarNotificationItem;
            }
        }

        /// <summary>
        /// Menu item used to show the clock
        /// </summary>
        private MenuItem ShowClockNotificationItem
        {
            get
            {
                if (this._ShowClockNotificationItem == null)
                {
                    this._ShowClockNotificationItem = new MenuItem();
                    this._ShowClockNotificationItem.Text = "Show Clock";
                    this.ShowClockNotificationItem.Click += new EventHandler(this.HandleShowClock);
                }

                return this._ShowClockNotificationItem;
            }
        }

        /// <summary>
        /// Menu item used to hide the clock
        /// </summary>
        private MenuItem HideClockNotificationItem
        {
            get
            {
                if (this._HideClockNotificationItem == null)
                {
                    this._HideClockNotificationItem = new MenuItem();
                    this._HideClockNotificationItem.Text = "Hide Clock";
                    this._HideClockNotificationItem.Click += new EventHandler(this.HandleHideClock);
                }

                return this._HideClockNotificationItem;
            }
        }

        /// <summary>
        /// Menu item used to exit the application
        /// </summary>
        private MenuItem ExitNotificationItem
        {
            get
            {
                if (this._ExitNotificationItem == null)
                {
                    this._ExitNotificationItem = new MenuItem();
                    this.ExitNotificationItem.Text = "Exit";
                    this.ExitNotificationItem.Click += new EventHandler(this.ExitNotificationItem_Click);
                }

                return this._ExitNotificationItem;
            }
        }

        /// <summary>
        /// List containing the individual log messages
        /// </summary>
        private List<String> Logs
        {
            get
            {
                if (this._Logs == null)
                {
                    this._Logs = new List<string>();
                }

                return this._Logs;
            }
        }
    }
}
