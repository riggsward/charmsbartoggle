﻿namespace CharmsBarToggle
{
    partial class ToggleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToggleForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.HideBarRadio = new System.Windows.Forms.RadioButton();
            this.ShowBarRadio = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.HideClockRadio = new System.Windows.Forms.RadioButton();
            this.ShowClockRadio = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.HideAllRadio = new System.Windows.Forms.RadioButton();
            this.ShowAllRadio = new System.Windows.Forms.RadioButton();
            this.HideBarAfterStartupCheckbox = new System.Windows.Forms.CheckBox();
            this.HideClockAfterStartupCheckbox = new System.Windows.Forms.CheckBox();
            this.RunAtStartupCheckbox = new System.Windows.Forms.CheckBox();
            this.MinimizeAfterStartupCheckbox = new System.Windows.Forms.CheckBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.VerticalSeparator = new System.Windows.Forms.GroupBox();
            this.LogTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.HideBarRadio);
            this.groupBox1.Controls.Add(this.ShowBarRadio);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(164, 45);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Charms Bar";
            // 
            // HideBarRadio
            // 
            this.HideBarRadio.AutoSize = true;
            this.HideBarRadio.Location = new System.Drawing.Point(65, 22);
            this.HideBarRadio.Name = "HideBarRadio";
            this.HideBarRadio.Size = new System.Drawing.Size(47, 17);
            this.HideBarRadio.TabIndex = 1;
            this.HideBarRadio.TabStop = true;
            this.HideBarRadio.Text = "Hide";
            this.HideBarRadio.UseVisualStyleBackColor = true;
            this.HideBarRadio.Click += new System.EventHandler(this.HandleHideBar);
            // 
            // ShowBarRadio
            // 
            this.ShowBarRadio.AutoSize = true;
            this.ShowBarRadio.Location = new System.Drawing.Point(7, 20);
            this.ShowBarRadio.Name = "ShowBarRadio";
            this.ShowBarRadio.Size = new System.Drawing.Size(52, 17);
            this.ShowBarRadio.TabIndex = 0;
            this.ShowBarRadio.TabStop = true;
            this.ShowBarRadio.Text = "Show";
            this.ShowBarRadio.UseVisualStyleBackColor = true;
            this.ShowBarRadio.Click += new System.EventHandler(this.HandleShowBar);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.HideClockRadio);
            this.groupBox2.Controls.Add(this.ShowClockRadio);
            this.groupBox2.Location = new System.Drawing.Point(13, 64);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(164, 45);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Charms Clock and Date";
            // 
            // HideClockRadio
            // 
            this.HideClockRadio.AutoSize = true;
            this.HideClockRadio.Location = new System.Drawing.Point(66, 20);
            this.HideClockRadio.Name = "HideClockRadio";
            this.HideClockRadio.Size = new System.Drawing.Size(47, 17);
            this.HideClockRadio.TabIndex = 1;
            this.HideClockRadio.TabStop = true;
            this.HideClockRadio.Text = "Hide";
            this.HideClockRadio.UseVisualStyleBackColor = true;
            this.HideClockRadio.Click += new System.EventHandler(this.HandleHideClock);
            // 
            // ShowClockRadio
            // 
            this.ShowClockRadio.AutoSize = true;
            this.ShowClockRadio.Location = new System.Drawing.Point(7, 20);
            this.ShowClockRadio.Name = "ShowClockRadio";
            this.ShowClockRadio.Size = new System.Drawing.Size(52, 17);
            this.ShowClockRadio.TabIndex = 0;
            this.ShowClockRadio.TabStop = true;
            this.ShowClockRadio.Text = "Show";
            this.ShowClockRadio.UseVisualStyleBackColor = true;
            this.ShowClockRadio.Click += new System.EventHandler(this.HandleShowClock);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.HideAllRadio);
            this.groupBox3.Controls.Add(this.ShowAllRadio);
            this.groupBox3.Location = new System.Drawing.Point(13, 116);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(164, 48);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "All";
            // 
            // HideAllRadio
            // 
            this.HideAllRadio.AutoSize = true;
            this.HideAllRadio.Location = new System.Drawing.Point(66, 20);
            this.HideAllRadio.Name = "HideAllRadio";
            this.HideAllRadio.Size = new System.Drawing.Size(47, 17);
            this.HideAllRadio.TabIndex = 1;
            this.HideAllRadio.TabStop = true;
            this.HideAllRadio.Text = "Hide";
            this.HideAllRadio.UseVisualStyleBackColor = true;
            this.HideAllRadio.Click += new System.EventHandler(this.HandleHideAll);
            // 
            // ShowAllRadio
            // 
            this.ShowAllRadio.AutoSize = true;
            this.ShowAllRadio.Location = new System.Drawing.Point(7, 20);
            this.ShowAllRadio.Name = "ShowAllRadio";
            this.ShowAllRadio.Size = new System.Drawing.Size(52, 17);
            this.ShowAllRadio.TabIndex = 0;
            this.ShowAllRadio.TabStop = true;
            this.ShowAllRadio.Text = "Show";
            this.ShowAllRadio.UseVisualStyleBackColor = true;
            this.ShowAllRadio.Click += new System.EventHandler(this.HandleShowAll);
            // 
            // HideBarAfterStartupCheckbox
            // 
            this.HideBarAfterStartupCheckbox.AutoSize = true;
            this.HideBarAfterStartupCheckbox.Location = new System.Drawing.Point(13, 218);
            this.HideBarAfterStartupCheckbox.Name = "HideBarAfterStartupCheckbox";
            this.HideBarAfterStartupCheckbox.Size = new System.Drawing.Size(154, 17);
            this.HideBarAfterStartupCheckbox.TabIndex = 3;
            this.HideBarAfterStartupCheckbox.Text = "Hide Charms Bar on Statup";
            this.HideBarAfterStartupCheckbox.UseVisualStyleBackColor = true;
            this.HideBarAfterStartupCheckbox.CheckedChanged += new System.EventHandler(this.HandleRebuildStartupValue);
            // 
            // HideClockAfterStartupCheckbox
            // 
            this.HideClockAfterStartupCheckbox.AutoSize = true;
            this.HideClockAfterStartupCheckbox.Location = new System.Drawing.Point(13, 241);
            this.HideClockAfterStartupCheckbox.Name = "HideClockAfterStartupCheckbox";
            this.HideClockAfterStartupCheckbox.Size = new System.Drawing.Size(165, 17);
            this.HideClockAfterStartupCheckbox.TabIndex = 4;
            this.HideClockAfterStartupCheckbox.Text = "Hide Charms Clock on Statup";
            this.HideClockAfterStartupCheckbox.UseVisualStyleBackColor = true;
            this.HideClockAfterStartupCheckbox.CheckedChanged += new System.EventHandler(this.HandleRebuildStartupValue);
            // 
            // RunAtStartupCheckbox
            // 
            this.RunAtStartupCheckbox.AutoSize = true;
            this.RunAtStartupCheckbox.Location = new System.Drawing.Point(13, 172);
            this.RunAtStartupCheckbox.Name = "RunAtStartupCheckbox";
            this.RunAtStartupCheckbox.Size = new System.Drawing.Size(95, 17);
            this.RunAtStartupCheckbox.TabIndex = 5;
            this.RunAtStartupCheckbox.Text = "Run at Startup";
            this.RunAtStartupCheckbox.UseVisualStyleBackColor = true;
            this.RunAtStartupCheckbox.CheckedChanged += new System.EventHandler(this.RunAtStartupCheckbox_CheckedChanged);
            // 
            // MinimizeAfterStartupCheckbox
            // 
            this.MinimizeAfterStartupCheckbox.AutoSize = true;
            this.MinimizeAfterStartupCheckbox.Location = new System.Drawing.Point(13, 195);
            this.MinimizeAfterStartupCheckbox.Name = "MinimizeAfterStartupCheckbox";
            this.MinimizeAfterStartupCheckbox.Size = new System.Drawing.Size(115, 17);
            this.MinimizeAfterStartupCheckbox.TabIndex = 6;
            this.MinimizeAfterStartupCheckbox.Text = "Minimize at Startup";
            this.MinimizeAfterStartupCheckbox.UseVisualStyleBackColor = true;
            this.MinimizeAfterStartupCheckbox.CheckedChanged += new System.EventHandler(this.HandleRebuildStartupValue);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Charms Bar Toggle";
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // VerticalSeparator
            // 
            this.VerticalSeparator.Location = new System.Drawing.Point(184, 12);
            this.VerticalSeparator.Name = "VerticalSeparator";
            this.VerticalSeparator.Size = new System.Drawing.Size(2, 246);
            this.VerticalSeparator.TabIndex = 7;
            this.VerticalSeparator.TabStop = false;
            // 
            // LogTextBox
            // 
            this.LogTextBox.BackColor = System.Drawing.Color.White;
            this.LogTextBox.Location = new System.Drawing.Point(193, 12);
            this.LogTextBox.Multiline = true;
            this.LogTextBox.Name = "LogTextBox";
            this.LogTextBox.ReadOnly = true;
            this.LogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.LogTextBox.Size = new System.Drawing.Size(385, 246);
            this.LogTextBox.TabIndex = 8;
            this.LogTextBox.WordWrap = false;
            // 
            // ToggleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(590, 267);
            this.Controls.Add(this.LogTextBox);
            this.Controls.Add(this.VerticalSeparator);
            this.Controls.Add(this.MinimizeAfterStartupCheckbox);
            this.Controls.Add(this.RunAtStartupCheckbox);
            this.Controls.Add(this.HideClockAfterStartupCheckbox);
            this.Controls.Add(this.HideBarAfterStartupCheckbox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(606, 305);
            this.MinimumSize = new System.Drawing.Size(606, 305);
            this.Name = "ToggleForm";
            this.Text = "Charms Bar Toggle";
            this.Load += new System.EventHandler(this.ToggleForm_Load);
            this.Resize += new System.EventHandler(this.ToggleForm_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton HideBarRadio;
        private System.Windows.Forms.RadioButton ShowBarRadio;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton ShowClockRadio;
        private System.Windows.Forms.RadioButton HideClockRadio;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton HideAllRadio;
        private System.Windows.Forms.RadioButton ShowAllRadio;
        private System.Windows.Forms.CheckBox HideBarAfterStartupCheckbox;
        private System.Windows.Forms.CheckBox HideClockAfterStartupCheckbox;
        private System.Windows.Forms.CheckBox RunAtStartupCheckbox;
        private System.Windows.Forms.CheckBox MinimizeAfterStartupCheckbox;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.GroupBox VerticalSeparator;
        private System.Windows.Forms.TextBox LogTextBox;

    }
}

