﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CharmsBarToggle.Charms
{
    /// <summary>
    /// Base class that contains the neccessary dll calls to get and set basic
    /// window properties like visibility
    /// </summary>
    class CharmsBase
    {
        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, WindowShowStyle nCmdShow);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        private struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        /// <summary>
        /// Window show commands that should be considered visible
        /// </summary>
        protected int[] _ShowCommandsConsideredVisible;

        /// <summary>
        /// Window show commands that should be considered visible
        /// </summary>
        public int[] ShowCommandsConsideredVisible {
            get {
                if (this._ShowCommandsConsideredVisible == null) {
                    this._ShowCommandsConsideredVisible = new int[] {
                        (int)WindowShowStyle.Show,
                        (int)WindowShowStyle.ShowNormal,
                        (int)WindowShowStyle.ShowMaximized,
                        (int)WindowShowStyle.ShowNormalNoActivate,
                        (int)WindowShowStyle.ShowNoActivate
                    };
                }

                return this._ShowCommandsConsideredVisible;
            }
            set {
                this._ShowCommandsConsideredVisible = value;
            }
        }

        /// <summary>
        /// Name of the window being tracked.
        /// </summary>
        protected String _WindowName;

        /// <summary>
        /// Name of the window being tracked.
        /// </summary>
        public String WindowName
        {
            get
            {
                if (this._WindowName == null)
                {
                    this._WindowName = "";
                }

                return this._WindowName;
            }
            set
            {
                this._WindowName = value;
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the Charms Bar that can be swipped in from the user.
        /// </summary>
        /// <remarks>
        /// The pointer to the Charm Bar window will be retrieved every time the visibility is set or retrieved.
        /// If the Window Handle cannot be found, the window is assumed to be visible.
        /// </remarks>
        public Boolean Visible
        {
            get
            {
                IntPtr hWndBar = FindWindowByCaption(IntPtr.Zero, this.WindowName);

                if (hWndBar != null && hWndBar != IntPtr.Zero)
                {
                    WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
                    GetWindowPlacement(hWndBar, ref placement);

                    if (Array.IndexOf(this.ShowCommandsConsideredVisible, placement.showCmd) != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
            set
            {
                Boolean currentVisibility = this.Visible;

                if (value != currentVisibility)
                {
                    IntPtr hWndBar = FindWindowByCaption(IntPtr.Zero, this.WindowName);

                    if (hWndBar != null && hWndBar != IntPtr.Zero)
                    {
                        if (value)
                        {
                            //Console.WriteLine(String.Format("Showing[{0}]:{1}", this.WindowName, hWndBar));
                            ShowWindow(hWndBar, WindowShowStyle.ShowNormal);
                        }
                        else
                        {
                            //Console.WriteLine(String.Format("Hiding[{0}]:{1}", this.WindowName, hWndBar));
                            ShowWindow(hWndBar, WindowShowStyle.ShowMinimized);
                        }
                    }
                }
            }
        }
    }
}
