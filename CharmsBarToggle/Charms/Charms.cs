﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharmsBarToggle.Charms
{
    /// <summary>
    /// Contains static references to the bar and clock interfaces.
    /// </summary>
    class Charms
    {
        /// <summary>
        /// Interacts with the Charms Bar.
        /// </summary>
        protected static TrackedCharmsBase _Bar;

        /// <summary>
        /// Interacts with the Charms Bar.
        /// </summary>
        public static TrackedCharmsBase Bar
        {
            get
            {
                if (_Bar == null)
                {
                    _Bar = new TrackedCharmsBase();
                    _Bar.WindowName = "Charm Bar";
                }

                return _Bar;
            }
        }

        /// <summary>
        /// Interacts with the Charms Clock.
        /// </summary>
        protected static TrackedCharmsBase _Clock;

        /// <summary>
        /// Interacts with the Charms Clock.
        /// </summary>
        public static TrackedCharmsBase Clock
        {
            get
            {
                if (_Clock == null)
                {
                    _Clock = new TrackedCharmsBase();
                    _Clock.WindowName = "Clock and Date";
                }

                return _Clock;
            }
        }
    }
}
