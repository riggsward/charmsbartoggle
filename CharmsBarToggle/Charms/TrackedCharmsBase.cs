﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Reflection;

namespace CharmsBarToggle.Charms
{
    /// <summary>
    /// Handler for when a property being tracked changes
    /// </summary>
    /// <param name="sender">Object dispatching the event</param>
    /// <param name="propertyName">Name of the property that changed</param>
    /// <param name="oldValue">Previous value of the property</param>
    /// <param name="newValue">Current value of the property</param>
    public delegate void PropertyChangedEventHandler(object sender, String propertyName, Object oldValue, Object newValue);

    /// <summary>
    /// Adds a timer to check if the visibility changed for the base
    /// </summary>
    class TrackedCharmsBase : CharmsBase
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Dictionary that contains the name of properties to watch and their preview values.
        /// </summary>
        protected Dictionary<String, Object> _PropertiesToWatch;

        /// <summary>
        /// Dictionary that contains the name of properties to watch and their preview values.
        /// </summary>
        public Dictionary<String, Object> PropertiesToWatch
        {
            get
            {
                if (this._PropertiesToWatch == null)
                {
                    this._PropertiesToWatch = new Dictionary<string, object>();
                    this.PropertiesToWatch.Add("Visible", true);
                }

                return this._PropertiesToWatch;
            }
        }

        /// <summary>
        /// Timer used to poll properties to see if their values have changed.
        /// </summary>
        protected Timer _PropertyTimer;

        /// <summary>
        /// Timer used to poll properties to see if their values have changed.
        /// </summary>
        public Timer PropertyTimer
        {
            get
            {
                if (this._PropertyTimer == null)
                {
                    this._PropertyTimer = new Timer();
                    this.PropertyTimer.Interval = 1000 / 60;
                    this.PropertyTimer.AutoReset = true;
                    this.PropertyTimer.Elapsed += new ElapsedEventHandler(this.HandlePropertyTimerElapsed);
                    this.PropertyTimer.Start();
                }

                return this._PropertyTimer;
            }
        }

        public TrackedCharmsBase()
        {
            if (this.PropertyTimer != null) { };
            if (this.PropertiesToWatch != null) { };
        }

        /// <summary>
        /// Handles when the property timer ellapses to check the property values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HandlePropertyTimerElapsed(Object sender, ElapsedEventArgs e)
        {
            PropertyInfo property;
            Object currentValue;
            Object oldValue;

            foreach (String propertyName in this.PropertiesToWatch.Keys.ToArray())
            {
                property = this.GetType().GetProperty(propertyName);

                if (property != null)
                {
                    currentValue = property.GetMethod.Invoke(this, null);
                    oldValue = this.PropertiesToWatch[propertyName];

                    if (!currentValue.Equals(oldValue))
                    {
                        this.PropertiesToWatch[propertyName] = currentValue;
                        this.OnPropertyChanged(propertyName, oldValue, currentValue);
                    }
                }
            }
        }

        /// <summary>
        /// Calls the property changed handles for the given property
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name, Object oldValue, Object newValue)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, name, oldValue, newValue);
            }
        }
    }
}
