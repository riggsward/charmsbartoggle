﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CharmsBarToggle
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool startupHideBar = false;
            bool startupHideClock = false;
            bool startupAutoMinimize = false;

            String arg;
            for (int i = 0; i < args.Length; i++ )
            {
                arg = args[i];

                if (arg == ToggleForm.HIDE_BAR_ON_STARTUP_FLAG)
                {
                    startupHideBar = true;
                }
                else if (arg == ToggleForm.HIDE_CLOCK_ON_STARTUP_FLAG)
                {
                    startupHideClock = true;
                }
                else if (arg == ToggleForm.AUTO_MINIMIZE_AFTER_STARTUP_FLAG)
                {
                    startupAutoMinimize = true;
                }
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ToggleForm(startupHideBar, startupHideClock, startupAutoMinimize));
        }
    }
}
